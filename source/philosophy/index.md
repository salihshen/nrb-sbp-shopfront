---
title: Filosofin
date: 2017-02-18 08:22:00
contribute_url: https://gitlab.com/swe-nrb/nrb-sbp-shopfront/blob/master/source/philosophy/index.md
---

> Visionen bakom "Smart Built Platform" är att skapa ett __modernt ekosystem__
> för att skapa, förvalta och distribuera dokument

### Öppenhet  

Har du någon gång irriterat dig på inaktuella dokument som ingen uppdaterar?
Vill du föreslå en förändring men du vet inte vem som "äger" dokumentet? Vi tror
oss ha lösningen på dessa problem.  

Genom att samarbeta med informationen på ett liknande sätt som utvecklare
samarbetar med programkod kan vi på ett öppet och kvalitetsäkert sätt samarbeta
kring dokument. Vi får dessutom tillgång till kraftfulla verktyg för automatiskt
distribution, versionshantering, kvalitetsäkring m.m.

Vi tror på ständiga förbättringar och vi har därför valt att publicera allt
material som "öppet data", det betyder att vem som helst kan använda, förbättra
och kopiera materialet.

### Ett ekosystem  

Ett öppet system där alla kan bidra ställer höga krav på transparans och spårbarhet
och därför bygger SBP på ett modulärt koncept där varje del paketeras och har en 
tydlig funktion i ekosystemet.

> SBP bygger på ett modulärt koncept där varje del har en tydlig funktion
> och bidrag till ekosystemet.

![Illustration över ekosystemet](ekosystem.svg)

### Läs mer  

Nedan har vi samlat mer detaljerad information om ekosystemet

* [Informationspaket](./information-package)
* Byggpaket (under uppbyggnad)
* Versionshantering (under uppbyggnad)

