---
title: Använda
date: 2017-02-18 08:22:08
contribute_url: https://gitlab.com/swe-nrb/nrb-sbp-shopfront/blob/master/source/use/index.md
---

Det är enkelt att nyttja resultatet av ett informationspaket. I tabellen nedan
finns enkla exempel på informationspaket.

| Namn                                                                                          | Nedladdningslänk                                                                                                                |
|-----------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------|
| [sbp-begreppslista](https://gitlab.com/swe-nrb/sbp-begreppslista)                             |                                                                                                                                 |
| [demo-infopack-rollbeskrivningar](https://gitlab.com/swe-nrb/demo-infopack-rollbeskrivningar) | [Ladda ner](https://gitlab.com/swe-nrb/demo-infopack-rollbeskrivningar/builds/artifacts/master/download?job=build_deliverables) |
