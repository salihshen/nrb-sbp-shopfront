---
title: Påverka
date: 2017-02-18 08:22:13
contribute_url: https://gitlab.com/swe-nrb/nrb-sbp-shopfront/blob/master/source/contribute/index.md
---

# Förändringsprocessen  

Det finns inga krav på att versionshantera ett informationspaket men det finns stora fördelar med att göra det. Nedan visar vi ett exempel på det arbetsflöde vi följer i [Gitlab](https://www.gitlab.com).

![Illustrationen visar projektets förändringsprocess](grafik/forandringsprocess.svg)
